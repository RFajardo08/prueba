package pruebas;

public class Prueba {

	public static void main(String[] args) {
		
		int[] casas = {1,1,1,0,1,1,0,0};
		int dias = 2;
		ActivaCasas(dias, casas);
		
		int[] cajas = {10, 60, 40, 35, 20};
		int camion = 130;
		CargarCamion(camion, cajas);
	}
	
	public static void ActivaCasas(int dias, int[] casas) {
		
		if(!(dias <= 0) && (casas.length == 8)) {
			for(int i=0; i < dias; i++) {
				int anterior = 0;
				int siguiente = 0;
				for(int j=0; j<casas.length; j++) {
					if(j == 7) {siguiente = 0;} else {siguiente = casas[j+1];}
					if(anterior == siguiente) {
						anterior = casas[j];
						casas[j] = 0;
					}else {
						anterior = casas[j];
						casas[j] = 1;
					}
				}
			}
			
			String salida = "[";
			for (int j=0; j<casas.length; j++) {
				if(j==7) {
					salida = salida + casas[j] + "]";
				}else {
					salida = salida + casas[j] + ",";
				}
			}
			
			System.out.println(salida);
			
		}else {System.out.println("Debe ingresar el numero de dias a calcular y el valor de 8 casas");}
		
	}

	public static void CargarCamion(int camion, int[] cajas) {
		camion = camion-30;
		String salida = "";
		int maximo = 0;
		int actual;
		for(int i=0; i<cajas.length; i++) {
			for(int j=i+1; j<cajas.length; j++) {
				actual = cajas[i]+cajas[j];
				if(actual <= camion) {
					if(actual > maximo) {
						maximo = actual;
						salida = "["+cajas[i]+","+cajas[j]+"]";
					}
				}
			}
		}
		System.out.println(salida);
	}
}
